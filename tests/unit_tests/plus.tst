// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

// DIFF + DIFF
x=0;
xdx=diffcode_der(x,1);
A=[1 1 -0.5;1 xdx xdx;0.5 0.5 0.5];
B=[xdx 2 0.5;1 1 xdx;0.5 xdx 0.5];
C = A+B;
Ev = [
1 3 0
2 1 0
1 0.5 1
];
Edv = [
1 0 0
0 1 2
0 1 0
];
assert_checkequal(C.v,Ev);
assert_checkequal(C.dv,Edv);
// DIFF + double
x=0;
xdx=diffcode_der(x,1);
A=[1 1 -0.5;1 xdx xdx;0.5 0.5 0.5];
B=[1 1 -0.5;1 -2 -3;0.5 0.5 0.5];
C = A+B;
Ev = [
2 2 -1
2 -2 -3
1 1 1
];
Edv = [
0 0 0
0 1 1
0 0 0
];
assert_checkequal(C.v,Ev);
assert_checkequal(C.dv,Edv);
// double + DIFF
C = B+A;
assert_checkequal(C.v,Ev);
assert_checkequal(C.dv,Edv);

