// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->


// function to minimize
function f=rosenbrock(x)
  f=1+sum( 100*(x(2:$)-x(1:$-1)^2)^2 + (1-x(2:$))^2)
endfunction

// Gradient for 2 variables
function g=rosenbrockG2(x)
  g(1) = -400*x(1)*(x(2)-x(1)^2)
  g(2) = 200*(x(2)-x(1)^2)-2*(1-x(2))
endfunction

//
x = [1;2];
[f,g,ind]=diffcode_CDcost(x,1,rosenbrock);
assert_checkalmostequal(f,rosenbrock(x));
assert_checkalmostequal(g,rosenbrockG2(x));
assert_checkequal(ind,1);
//
// Test with optim
[fopt,xopt,gopt]=optim(list(diffcode_CDcost,rosenbrock),[1;2;3;4]);
assert_checkalmostequal(fopt,1);
assert_checkalmostequal(xopt,[1;1;1;1]);
assert_checkalmostequal(gopt,[0;0;0;0],[],1.e-8);

//same problem defined using a loop
function f=rosenbrock1(x)
  f=1
  for k=2:size(x,"*")
    f=f+100*(x(k)-x(k-1)^2)^2 + (1-x(k))^2
  end
endfunction

[fopt,xopt,gopt]=optim(list(diffcode_CDcost,rosenbrock1),[1;2;3;4]);
assert_checkalmostequal(fopt,1);
assert_checkalmostequal(xopt,[1;1;1;1]);
assert_checkalmostequal(gopt,[0;0;0;0],[],1.e-8);

// function to minimize
function f=rosenbrock2(x,a)
  f=1+sum( a*(x(2:$)-x(1:$-1)^2)^2 + (1-x(2:$))^2)
endfunction
// Gradient for 2 variables
function g=rosenbrockG2p(x,a)
  g(1) = -4*a*x(1)*(x(2)-x(1)^2)
  g(2) = 2*a*(x(2)-x(1)^2)-2*(1-x(2))
endfunction
//
x = [1;2];
p = 200;
[f,g,ind]=diffcode_CDcost(x,1,rosenbrock2,p);
assert_checkalmostequal(f,rosenbrock2(x,p));
assert_checkalmostequal(g,rosenbrockG2p(x,p));
assert_checkequal(ind,1);
//
// With optim
x0=[1;2;3;4];
[f,xopt,gopt]=optim(list(diffcode_CDcost,rosenbrock2,p),x0);
assert_checkalmostequal(fopt,1);
assert_checkalmostequal(xopt,[1;1;1;1]);
assert_checkalmostequal(gopt,[0;0;0;0],[],1.e-8);
