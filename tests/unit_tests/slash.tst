// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

// DIFF / DIFF
Av = [
-2.5 3 -7
-15 9 -20.5
10.5 0 3.25
];
Adv = [
3 -1 0.5
0 -5.5 0
0.5 6.5 -6
];
A=diffcode_der(Av,Adv);
x=0;
xdx=diffcode_der(x,1);
B=[3 xdx 1;0.5 3 -6;3 xdx 0.5];
C = A/B;
Ev = [
-1 1 0
0.5 3 -6
3 0 0.5
];
Edv = [
0 0 1
0 0 0
0 1 0
];
assert_checkalmostequal(C.v,Ev,[],1.e-13);
assert_checkalmostequal(C.dv,Edv,[],1.e-13);
// DIFF / double
Av = [
-2.5 3 -7
-15 9 -20.5
10.5 0 3.25
];
Adv = [
3 -1 0.5
0 -5.5 0
0.5 6.5 -6
];
A=diffcode_der(Av,Adv);
B=[3 0 1;0.5 3 -6;3 0 0.5];
C = A/B;
Ev = [
-1 1 0
0.5 3 -6
3 0 0.5
];
Edv = [
-4.0555556 -0.3333333 5.1111111
-22.305556 -1.8333333 22.611111
14.194444 2.1666667 -14.388889
];
assert_checkalmostequal(C.v,Ev,1.e-6,1.e-13);
assert_checkalmostequal(C.dv,Edv,1.e-6,1.e-13);
// double / DIFF
Av = [
-2.5 3 -7
-15 9 -20.5
10.5 0 3.25
];
Adv = [
3 -1 0.5
0 -5.5 0
0.5 6.5 -6
];
A=diffcode_der(Av,Adv);
B=[3 0 1;0.5 3 -6;3 0 0.5];
C = B/A;
Ev = [
-0.0759494 0.0253165 0.3037975
0.9240506 0.0253165 0.3037975
0.4556962 -0.1518987 0.1772152
];
Edv = [
1.6273033 -0.7548114 -0.6836155
-0.7018106 0.1326710 -0.2560487
1.5020029 -0.6708505 -0.7393758
];
disp(abs(C.v - Ev));
assert_checkalmostequal(C.v,Ev,1.e-6,1.e-13);
assert_checkalmostequal(C.dv,Edv,1.e-6,1.e-13);
