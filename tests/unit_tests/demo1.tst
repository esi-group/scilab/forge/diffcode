// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

function y=foo(x)
  A=[1   1   0.5;
     1   x   x;
     0.5 0.5 0.5];
  y=A
endfunction

//standard evaluation
x=0;
y=foo(x);

//evaluation with gradient computation
xdx=diffcode_der(x,1)
ydy=foo(xdx);

//check gradient
h=0.000000001;
fp = (foo(x+h)-foo(x))/h;

assert_checkalmostequal(ydy.dv,fp);
