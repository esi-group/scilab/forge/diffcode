// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

// sum(DIFF)
x=0;
xdx=diffcode_der(x,1);
A=[1 1 -0.5;1 xdx xdx;0.5 0.5 0.5];
C = sum(A);
Ev = 4;
Edv = 2;
assert_checkequal(C.v,Ev);
assert_checkequal(C.dv,Edv);
// sum(DIFF,"r")
x=0;
xdx=diffcode_der(x,1);
A=[1 1 -0.5;1 xdx xdx;0.5 0.5 0.5];
C = sum(A,"r");
Ev = [2.5 1.5 0];
Edv = [0 1 1];
assert_checkequal(C.v,Ev);
assert_checkequal(C.dv,Edv);
// sum(DIFF,"c")
x=0;
xdx=diffcode_der(x,1);
A=[1 1 -0.5;1 xdx xdx;0.5 0.5 0.5];
C = sum(A,"c");
Ev = [
1.5
1
1.5
];
Edv = [
0
2
0
];
assert_checkequal(C.v,Ev);
assert_checkequal(C.dv,Edv);
