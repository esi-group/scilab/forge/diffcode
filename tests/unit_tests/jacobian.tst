// Copyright (C) 2011 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

// 1. Test with a scalar argument
function y = myfunction (x)
    y = x*x;
endfunction

x = 1.0;
expected = 2.0;
computed = diffcode_jacobian(myfunction,x);
assert_checkequal ( computed , expected );

// 2. Test with a vector argument
function y = myfunction2 (x)
    y = x(1)*x(1) + x(2)+ x(1)*x(2);
endfunction
x = [1.0;2.0];
expected = [4.;2.];
computed = diffcode_jacobian(myfunction2,x);
assert_checkequal ( computed , expected );


//
function y = myfunction3 (x,n)
    myn = 1.e5;
    y = x^(2/myn);
endfunction
x = 1.0;
myn = 1.e5;
expected = (2/myn) * x^(2/myn-1);
computed = diffcode_jacobian(myfunction3 , x );
assert_checkequal ( computed , expected );

//
// 7. Test vector output y
function y=myexample(x)
    f1 = sin(x(1)*x(2))+exp(x(2)*x(3)+x(1))
    f2 = sum(x.^3)
    y=[f1;f2]
endfunction
// The exact gradient
function J = exactg(x)
    g1(1) = cos(x(1)*x(2))*x(2)+exp(x(2)*x(3)+x(1))
    g1(2) = cos(x(1)*x(2))*x(1)+exp(x(2)*x(3)+x(1))*x(3)
    g1(3) = exp(x(2)*x(3)+x(1))*x(2)
    //
    g2(1) = 3*x(1)^2
    g2(2) = 3*x(2)^2
    g2(3) = 3*x(3)^2
    //
    J = [g1,g2]
endfunction
x=[1;2;3];
expected = exactg(x);
computed = diffcode_jacobian(myexample,x);
assert_checkequal ( computed , expected );
//
// 8. Check the number of function evaluations
function y = myFevalFun(x)
    global FEVAL
    FEVAL = FEVAL + 1
    y = sum(x.^3)
endfunction
n = 3;
x = ones(n,1);
global FEVAL;
FEVAL = 0;
g = diffcode_jacobian(myFevalFun, x);
assert_checkequal ( FEVAL, 3);
//
// 9. Check with extra-arguments
function y=G(x,p) 
    f1 = sin(x(1)*x(2)*p)+exp(x(2)*x(3)+x(1)) 
    f2 = sum(x.^3)
    y=[f1; f2]
endfunction
x=[1;2;3];
expected = exactg(x);
p=1;
computed=diffcode_jacobian(list(G,p),x);
assert_checkequal ( computed , expected );

