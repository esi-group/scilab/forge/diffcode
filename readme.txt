Diffcode toolbox

Purpose
----

This toolbox provides forward automatic differentiation.
It is based on operators and functions overloading.

Given a Scilab code computing a variable y depending on a variable x
and a direction dx it allow evaluation of y together with the
directional derivative Grad(y)*dx.

The module supports common arithmetic operations, common elementary functions and
several matrix functions, including matrix inversion.

Type :

help diffcode_overview

for a quick start.

Features
--------

The following is a list of the current functions :
 * diffcode_der -  Create a new code differentiation object.
 * diffcode_CDcost -  Objective function for optim.
 * diffcode_hessian — Compute the Hessian of the function.
 * diffcode_jacobian — Compute the Jacobian of the function.


Dependencies
------------

 * This module depends on the helptbx module.
 * This module depends on the assert module.
 * This module depends on the apifun module.

TODO
----

 * Create min, max, prod
 * Create isreal, imag, imult
 * Create eig
 * Create .\
 * Create ~
 * Create VDV.^SDS, where SDS is a scalar (fun,der) pair.
 * Create .'
 * Create &
 * Create ./., .\.
 * Fix the A^B derivative with respect to B
   when A is matrix, B is scalar.
 * Test conj
 * Test abs, cumprod, cumsum, matrix, maxi, mini, pinv, prod.

History
-------

This toolbox was first released at:

http://www.scilab.org/contrib/index_contrib.php?page=displayContribution&fileID=186

in August 2002.

Michael Baudin updated the module for Scilab v5.

Authors
----

 * Copyright (C) 2012 - Michael Baudin
 * Copyright (C) 2011 - DIGITEO - Michael Baudin
 * Copyright (C) 2002 - INRIA - Xavier Jonsson
 * Copyright (C) 2002 - 2009 - INRIA - Serge Steer

Licence
----

This toolbox is released under the CeCILL_V2 licence :

http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

Bibliography
------------

 * "The matrix cookbook", Kaare Brandt Petersen, Michael Syskind Pedersen, 2008
 * "Matrix Differential Calculus with Applications in Statistics and Econometrics", Jan R. Magnus, Heinz Neudecker, 2007
 * "Symbolic matrix derivatives", Paul S. Dwyer, M.S. Macphail, Ann. Math. Statist. Volume 19, Number 4 (1948), 517-534.

