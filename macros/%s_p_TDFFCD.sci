// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [z]=%s_p_TDFFCD (x,y)
    v = x^y.v
	if ( or(y.dv<>0) ) then
	if ( and(size(x)<>[1 1]) )
    // TODO : fix the derivative with respect to y, 
	// when x is a square matrix.
	// The formula below (with log) is correct only elementwise.
	error(msprintf("%s: Cannot differentiate A^B with respect to B.","%TDFFCD_p_TDFFCD"))
	end
	end
    dv = log(x).*x^y.v.*y.dv
  z = diffcode_der(v,dv)
endfunction
