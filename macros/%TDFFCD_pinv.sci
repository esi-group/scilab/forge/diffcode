// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function [z]=%TDFFCD_pinv(x)
  pinvx = pinv(x.v);
  [m,n] = size(x.v);
  if m < n then
    if rank(x.v) == m then
      xxt = x.v*x.v';  //'
      dpinvx = xxt\(x.dv-(x.dv*x.v'+x.v*x.dv')'*pinvx'); 
      dpinvx = dpinvx'; //'
    end
  else
    if rank(x.v) == n then
      xtx = x.v'*x.v; //'
      dpinvx = xtx \ (x.dv'-(x.dv'*x.v+x.v'*x.dv)*pinvx); //'
    end
  end
  z = diffcode_der(pinvx,dpinvx);
endfunction
