// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [xdx]=diffcode_der(x,dx)
    // Creates a new diffcode object.

    //
    // Check input arguments
    [lhs,rhs]=argn();
    apifun_checkrhs ( "diffcode_der" , rhs , 2 )
    apifun_checklhs ( "diffcode_der" , lhs , 1 )
    //
    // Check type
    apifun_checktype ( "diffcode_der" , x , "x" , 1 , ["constant" "hypermat" "TDFFCD"] )
    apifun_checktype ( "diffcode_der" , dx , "dx" , 2 , ["constant" "hypermat" "TDFFCD"] )
    //
    // Check sizes
    s = size(x)
    apifun_checkdims ( "diffcode_der" , dx , "dx" , 2 , s )
    //
    // Check content
    // Nothing to do.
    //
    xdx = mlist(["TDFFCD";"v";"dv"],x,dx)
endfunction
