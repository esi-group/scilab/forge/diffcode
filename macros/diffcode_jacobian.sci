// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function J = diffcode_jacobian(__diffcode_jacobian_f__,x)
    // Compute the Jacobian of the function.
    //
    // Calling Sequence
    //   J = diffcode_jacobian(f,x)
    //
    // Parameters
    //   f : a function or a list, the function to differenciate.
    //   x : a n-by-1 matrix of doubles, real, the point where to compute the derivatives
    //   J : a n-ny-m matrix of doubles, the exact Jacobian. The column <literal>J(:,k)</literal> approximates the gradient of <literal>fk</literal>, for <literal>k=1,2,...,m</literal>.
    //
    // Description
    // Computes the exact Jacobian matrix of the function.
    // The algorithm  uses an exact differentiation method.
    //
    // If <literal>f</literal> is a compiled function, then <literal>diffcode_jacobian</literal>
    // cannot differentiate <literal>f</literal>.
    //
    // The function must have header
    //
    // <screen>
    // y = f( x )
    // </screen>
    //
    // where
    // <itemizedlist>
    // <listitem><para>x is a n-by-1 matrix of doubles,</para></listitem>
    // <listitem><para>y is a m-by-1 matrix of doubles.</para></listitem>
    // </itemizedlist>
    //
    // It might happen that the function requires additionnal
    // arguments to be evaluated.
    // In this case, we can use the following feature.
    // The function <literal>f</literal> can also be the list <literal>(func,a1,a2,...)</literal>
    // where, <literal>func</literal>, the first element in the list,
    // must be a function with header
    //
    // <screen>
    // y = func ( x , a1 , a2 , ... ).
    // </screen>
    //
    // In this case, the input arguments <literal>a1, a2, ...</literal> will be automatically be appended at the
    // end of the calling sequence of <literal>func</literal>.
    //
    // The <literal>diffcode_jacobian</literal> function calls the function
    // <literal>f</literal> several times in order to compute its
    // derivatives.
    // The total function evaluations is <literal>n</literal> times, where <literal>n</literal>
    // is the size of <literal>x</literal>.
    //
    // Examples
    //  // The function to differentiate
    //  function y=f(x)
    //    f1 = sin(x(1)*x(2))+exp(x(2)*x(3)+x(1))
    //    f2 = sum(x.^3)
    //    y=[f1;f2]
    //  endfunction
    //  // The exact gradient
    //  function J = exactg(x)
    //    g1(1) = cos(x(1)*x(2))*x(2)+exp(x(2)*x(3)+x(1))
    //    g1(2) = cos(x(1)*x(2))*x(1)+exp(x(2)*x(3)+x(1))*x(3)
    //    g1(3) = exp(x(2)*x(3)+x(1))*x(2)
    //    //
    //    g2(1) = 3*x(1)^2
    //    g2(2) = 3*x(2)^2
    //    g2(3) = 3*x(3)^2
    //    //
    //    J(:,1)=g1
    //    J(:,2)=g2
    //  endfunction
    // // Compute the exact Jacobian
    // x=[1;2;3];
    // J = diffcode_jacobian(f,x)
    // Jexact = exactg(x)
    // and(J==Jexact)
    //
    // // Passing extra parameters
    // function y=G(x,p)
    //   f1 = sin(x(1)*x(2)*p)+exp(x(2)*x(3)+x(1))
    //   f2 = sum(x.^3)
    //   y=[f1; f2]
    // endfunction
    // p=1;
    // h=1e-3;
    // J=diffcode_jacobian(list(G,p),x)
    //
    // Authors
    // Michael Baudin, DIGITEO, 2011
    //

    //
    // Check input arguments
    [lhs,rhs]=argn();
    apifun_checkrhs ( "diffcode_jacobian" , rhs , 2 )
    apifun_checklhs ( "diffcode_jacobian" , lhs , 1 )
    //
    // Get input arguments
    //
    // Manage x, to get the size n.
    apifun_checktype ( "diffcode_jacobian" , x , "x" , 1 , "constant" )
    [n,p] = size(x)
    apifun_checkvector ( "diffcode_jacobian" , x , "x" , 1 )
    // Make x a column vector, if required
    if ( p<>1 ) then
        x = x(:)
        [n,p] = size(x)
    end
    //
    // Check types
    apifun_checktype ( "diffcode_jacobian" , __diffcode_jacobian_f__ , "f" , 2 , ["function" "list"])
    if type(__diffcode_jacobian_f__)==15 then
        // List case
        // Check that the first element in the list is a function
        apifun_checktype ( "diffcode_jacobian" , __diffcode_jacobian_f__(1) , "f" , 2 , "function" )
    end
    //
    // Check sizes
    if type(__diffcode_jacobian_f__)==15 then
        // List case
        if ( length(__diffcode_jacobian_f__) < 2 ) then
            error(msprintf(gettext("%s: Wrong size for input argument #%d: %d-element list expected.\n"),"diffcode_jacobian",2,2));
        end
    end
    //
    // Check value
    // Nothing to do.
    //
    // Proceed...
    dx=zeros(n,1)
    for k=1:n
        dx(k)=1
        F=numderivative_evalf(__diffcode_jacobian_f__,diffcode_der(x,dx))
        J(k,:)=F.dv'
        dx(k)=0
    end
endfunction
// numderivative_evalf --
// Computes the value of __numderivative_f__ at the point x.
// The argument __numderivative_f__ can be a function (macro or linked code) or a list.
function y=numderivative_evalf(__numderivative_f__,x)
    if type(__numderivative_f__)==15 then
        // List case
        __numderivative_fun__=__numderivative_f__(1);
        instr = "y=__numderivative_fun__(x,__numderivative_f__(2:$))"
    elseif or(type(__numderivative_f__)==[11 13]) then
        // Function case
        instr = "y=__numderivative_f__(x)"
    else
        error(msprintf(gettext("%s: Wrong type for input argument #%d: A function expected.\n"),"numderivative",1));
    end
    ierr=execstr(instr,"errcatch")
    if ierr <> 0 then
        lamsg = lasterror()
        lclmsg = "%s: Error while evaluating the function: ""%s""\n"
        error(msprintf(gettext(lclmsg),"numderivative",lamsg));
    end
endfunction
