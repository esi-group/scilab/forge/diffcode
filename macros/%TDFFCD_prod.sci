// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2009 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [z]=%TDFFCD_prod (a,orient)
  if argn(2)==1 then orient='*',end
  if orient=='m' then
    orient=find(size(arg)>1,1)
  end
  if orient=='*' then // prod(a)
    z=a(1);
    for k=2:size(a,'*')
      y=a(k)
      z=der(z.v*y.v,z.v*y.dv + z.dv*y.v)
    end
  else //prod(a,orient)
    if orient==1|orient=='r' then
      z=a(1,:);
      for k=2:size(a,orient)
	y=a(k,:)
	z=der(z.v.*y.v,z.v.*y.dv + z.dv.*y.v)
      end

    elseif orient==2|orient=='c' then
      z=a(:,1);
      for k=2:size(a,orient)
	y=a(:,k)
	z=der(z.v.*y.v,z.v.*y.dv + z.dv.*y.v)
      end
      
    end
    
  end
endfunction
