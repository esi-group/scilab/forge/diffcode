// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// Computes d(x^y).
// Note:
// * if x is square matrix and y is integer scalar, this is a 
//   matrix power,
// * if x is row or column vector and y is scalar, this is an 
//   elementwise power.
// * if x is a scalar and y is a matrix, this is an 
//   elementwise power.
function z = %TDFFCD_p_TDFFCD (x,y)
    // Load Internals lib
    path = get_function_path("%TDFFCD_p_TDFFCD")
    path = dirname(path)
    internallib  = lib(fullfile(path,"internals"))
    //
    v = x.v^y.v
    if ( or(y.dv<>0) ) then
        if ( and(size(x.v)<>[1 1]) )
            // TODO : fix the derivative with respect to y, 
            // when x is a square matrix.
            // The formula below (with log) is correct only elementwise.
            error(msprintf("%s: Cannot differentiate A^B with respect to B.","%TDFFCD_p_TDFFCD"))
        end
    end
    dv = log(x.v).*v.*y.dv + diffcode_matrixpow(x.v,x.dv,y.v)
    z = diffcode_der(v,dv)
endfunction
