// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// Computes d(V^k) with respect to V.
//
// Arguments
// v: a n-by-n matrix of doubles, the matrix
// dv: a n-by-n matrix of doubles, the derivative matrix
// k: a 1-by-1 matrix of doubles, integer value, the exponent
//
// Description
// Manages the case where k is a scalar integer and V is
//  * a 1-by-1 matrix,
//  * a n-by-1 matrix (elementwise pow),
//  * a 1-by-n matrix (elementwise pow),
//  * a n-by-n matrix (matrix pow).
// Also manage the case where k is a matrix, and
// V is a scalar.
//
// For k=2, we have:
// d(V^2) = dV*V + V*dV
// In general, dV*V is different from V*dV,
// so that d(V^2) is different from 2*V*dV.
// For k = 3, we have:
// d(V^3) = dV*V^2 + V*dV*V + V^2*dV
// For general k, we have:
// d(V^k) = d(V^(k-1))*V + V^(k-1)*dV

function dvk = diffcode_matrixpow(v, dv, k)
    [m, n] = size(v);
    if m <> 1 ..
     & n <> 1 ..
     & m <> n then
        lclmsg = "%s: Wrong size for input argument #%d: " ..
               + "Square matrix expected.\n";
        error(msprintf(gettext(lclmsg), "diffcode_matrixpow", 1));
    end
    if m <> n then
        // Vector^scalar case
        dvk = k*v.^(k-1).*dv;
        return;
    end
    if m == 1 ..
     & n == 1 then
        // Scalar ^ Matrix: elementwise pow
        dvk = k.*v.^(k-1).*dv;
        return;
    end
    if or(floor(k) <> k) then
        lclmsg = "%s: Wrong value for input argument #%d: " ..
               + "An integer value expected.\n";
        error(msprintf(gettext(lclmsg), "diffcode_matrixpow", 3));
    end
    // Matrix ^ Integer scalar
    if k < 0 then
        vk = v^(-k);
        dvk = diffcode_matrixpow(v, dv, -k);
        ivk = inv(vk);
        dvk = -ivk * dvk * ivk;
        return;
    end
    // Matrix ^ Positive integer scalar
    if k == 0 then
        dvk = eye(v);
    elseif k == 1 then
        dvk = dv;
    else
        dvk = dv;
        for i = 1 : k-1
            dvk = dvk*v + v^i*dv;
        end
    end
endfunction
