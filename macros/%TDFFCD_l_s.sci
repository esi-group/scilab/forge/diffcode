// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function [z]=%TDFFCD_l_s (x,y)

  A=x.v;dA=x.dv;
  X=A\y
  dX=A\((A')\(-(dA'*A+A'*dA)*X+dA'*y))
  z = diffcode_der(X,dX)
endfunction
