// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function z=%TDFFCD_e (varargin)
    // z = %TDFFCD_e (i1,i2,...,ik,x)
    // z = xdx(i1,i2,...,ik)
    x = varargin($)
    v = x.v(varargin(1:$-1))
    dv = x.dv(varargin(1:$-1))
    z = diffcode_der(v,dv)
endfunction
