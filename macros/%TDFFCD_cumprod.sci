// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2009 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [z]=%TDFFCD_cumprod (a,orient)
  if argn(2)==1 then orient='*',end
  if orient=='m' then
    orient=find(size(arg)>1,1)
  end
  da=a.dv;a=a.v; 
  z=cumprod(a,orient)
  dz=zeros(z)
  if orient=='*' then // prod(a)
    dz(1)=da(1)
    for k=2:size(a,orient)
      dz(k)=dz(k-1)*a(k)+da(k)*z(k-1)
    end
  else //prod(a,orient)
    if orient==1|orient=='r' then
      dz(1,:)=da(1,:);
      for k=2:size(a,orient)
	dz(k,:)=dz(k-1,:).*a(k,:)+da(k,:).*z(k-1,:)
      end
    elseif orient==2|orient=='c' then
          dz(:,1)=da(:,1);
      for k=2:size(a,orient)
	dz(:,k)=dz(:,k-1).*a(:,k)+da(:,k).*z(:,k-1)
      end

    end
    z=der(z,dz)
  end
endfunction
