// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//function to minimize
function f=rosenbrock(x)
  f=1+sum( 100*(x(2:$)-x(1:$-1)^2)^2 + (1-x(2:$))^2)
endfunction

[f,xopt,gopt]=optim(list(diffcode_CDcost,rosenbrock),[1;2;3;4])

//same problem defined using a loop
function f=rosenbrock1(x)
  f=1
  for k=2:size(x,"*")
    f=f+100*(x(k)-x(k-1)^2)^2 + (1-x(k))^2
  end
endfunction

[f,xopt,gopt]=optim(list(diffcode_CDcost,rosenbrock1),[1;2;3;4])

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "demo5.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
