// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y=foo3(x)
  A=[1 1 0.5;1 x x^3;0.5 0.5 (x+1)^2];
  //b=[-1;2.5;1.5^5-1] 
  b=[-1;x+1;x^5-1]
  y=A\b
endfunction

//standard evaluation
x=1.5;
y=foo3(x)

//evaluation with gradient computation
xdx=diffcode_der(x,1)
ydy=foo3(xdx)

//check gradient
h=1d-8;
ydy.dv-(foo3(x+h)-foo3(x))/h


//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "demo3.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
